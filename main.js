import 'ol/ol.css';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import View from 'ol/View';
import {
  Modify,
  Select,
  defaults as defaultInteractions,
  DragBox
} from 'ol/interaction';
import { OSM, Vector as VectorSource } from 'ol/source';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import Feature from 'ol/Feature';
import { Point } from 'ol/geom';
import { fromLonLat } from 'ol/proj';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style';
import { platformModifierKeyOnly } from 'ol/events/condition';

let map;
let pointsLayer;
let geoJsonLayer;
let selectedFeatures = [];
let pointsList = [];

(async function () {
  // Получаем объект GeoJSON
  const geoJsonResponse = await fetch(require("url:./geo.geojson"));

  // Слой картинки с картой
  const raster = new TileLayer({
    source: new OSM(),
  });

  // Получаем объект GeoJSON в JSON
  const geoJson = await geoJsonResponse.json();

  // Слой с GeoJSON
  geoJsonLayer = new VectorLayer({
    source: new VectorSource({
      url: geoJsonResponse.url,
      format: new GeoJSON(),
      // features: new GeoJSON().readFeatures(
      //   {
      //     'type': 'FeatureCollection',
      //     'features': [{
      //       'type': 'Feature',
      //       'geometry': geoJson
      //     }]
      //   }
      // ),
      wrapX: false,
    }),
  });

  geoJson.coordinates[0][0].forEach((pointCoordinates) => {
    pointsList.push(new Feature({
      'name': 'visualPoint',
      'geometry': new Point(fromLonLat(pointCoordinates))
    }));
  });

  pointsLayer = new VectorLayer({
    source: new VectorSource({
      features: pointsList,
      wrapX: false,
    })
  });

  renderSelectedPoints();

  const dragBox = new DragBox({
    condition: platformModifierKeyOnly,
  });

  // Создаем карту со всеми слоями
  map = new Map({
    interactions: defaultInteractions().extend([dragBox]),
    layers: [raster, geoJsonLayer],
    target: 'map',
    view: new View({
      center: fromLonLat([43.628965, 55.283182]),
      zoom: 15
    })
  });

  dragBox.on('boxend', function () {
    let extent = dragBox.getGeometry().getExtent();
    const boxFeatures = pointsLayer.getSource()
      .getFeaturesInExtent(extent)
      .filter((feature) => feature.getGeometry().intersectsExtent(extent));

    // features that intersect the box geometry are added to the
    // collection of selected features

    // // if the view is not obliquely rotated the box geometry and
    // // its extent are equalivalent so intersecting features can
    // // be added directly to the collection
    const rotation = map.getView().getRotation();
    const oblique = rotation % (Math.PI / 2) !== 0;

    // when the view is obliquely rotated the box extent will
    // exceed its geometry so both the box and the candidate
    // feature geometries are rotated around a common anchor
    // to confirm that, with the box geometry aligned with its
    // extent, the geometries intersect
    // if (oblique) {
    const anchor = [0, 0];
    const geometry = dragBox.getGeometry().clone();
    geometry.rotate(-rotation, anchor);
    extent = geometry.getExtent();
    boxFeatures.forEach(function (feature) {
      const geometry = feature.getGeometry().clone();
      geometry.rotate(-rotation, anchor);
      if (geometry.intersectsExtent(extent)) {
        selectedFeatures.push(feature);
      }
    });
    // } else {
    //   selectedFeatures.extend(boxFeatures);
    // }

    console.log(selectedFeatures);
    renderSelectedPoints();
  });

  // clear selection when drawing a new box and when clicking on the map
  dragBox.on('boxstart', function () {
    selectedFeatures = [];
  });

  map.on("click", function (e) {
    let foundFeatureOnPixel = false;
    map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
      const featureName = feature.get('name');
      foundFeatureOnPixel = true;
      if (featureName == 'visualPoint') {
        selectedFeatures.push(feature);
        renderSelectedPoints();
      }
    });
    if (!foundFeatureOnPixel) {
      selectedFeatures = [];
      renderSelectedPoints();
    }
  });
})()

let isPointsShow = false;

document.getElementById('showPoints').addEventListener('click', function () {
  if (isPointsShow) {
    map.removeLayer(pointsLayer);
    isPointsShow = false;
  } else {
    map.addLayer(pointsLayer);
    isPointsShow = true;
  }
  // console.log(geoJsonLayer.getSource().getFeatures()[0].getGeometry())
});

document.addEventListener("keydown", function (event) {
  if (event.key === "Delete") {
    let deletePoints = selectedFeatures.map((feature) => feature.getGeometry().getCoordinates());
    selectedFeatures.forEach((feature) => {
      pointsLayer.getSource().removeFeature(feature);
    });
    deletePointsFromMap(deletePoints);
    selectedFeatures = [];
  }
});

function renderSelectedPoints() {
  pointsList.forEach(feature => {
    feature.setStyle([
      new Style({
        image: new CircleStyle({
          radius: 3,
          fill: new Fill({ color: 'red' }),
          stroke: new Stroke({ color: 'red', width: 1 }),
        }),
      })
    ]);
  });

  selectedFeatures.forEach(feature => {
    feature.setStyle([
      new Style({
        image: new CircleStyle({
          radius: 3,
          fill: new Fill({ color: 'green' }),
          stroke: new Stroke({ color: 'green', width: 1 }),
        }),
      })
    ]);
  });
}

function deletePointsFromMap(deletePoints) {
  const feature = geoJsonLayer.getSource().getFeatures()[0];
  const polygon = feature.getGeometry();
  const coordinates = polygon.getCoordinates();
  console.log(coordinates);
  console.log(deletePoints)

  coordinates[0][0] = coordinates[0][0].filter(point => {
    let isFoundInDelete = deletePoints.find((deletePoint) => deletePoint[0] == point[0] && deletePoint[1] == point[1]);
    return !isFoundInDelete;
  });

  polygon.setCoordinates(coordinates);
}
